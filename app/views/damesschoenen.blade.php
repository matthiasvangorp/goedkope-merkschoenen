@extends('layouts.master')

@section('title')
    @parent
    @if ($hasCategory)
        {{$categoryName}} bij goedkope merkschoenen
    @else
        {{$title}} bij goedkope merkschoenen
    @endif
@stop

@section('content')

<div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
            <li class="active" id="overview"><a href="{{$baseUrl}}">Alle</a></li>
            @foreach($categories as $category)
                <li id="category_{{$category->id}}"><a href="{{$baseUrl}}{{$category->path}}">{{ucfirst($category->name)}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <div>
            <h1>{{$title}} <?php if ($hasCategory){echo " > ".ucfirst($categoryName);}?></h1>
            <div class="form">

                 @if($hasCategory == true)
                    {{ Form::open(array("url" => "$baseUrl$categoryPath")) }}
                @else
                    {{ Form::open(array("url" => rtrim($baseUrl,"/"))) }}
                @endif
                    <table class="filter">
                        <tr>
                            <td><?php echo Form::label('price', 'Van &euro; ');?></td>
                            <td><?php echo Form::number('priceFrom', $priceFrom); ?> <br/></td>
                            <td><?php echo Form::label('brand', 'Merk');?></td>
                            <td>
                                <select class="form-control" name="brandId">
                                    <option selected="selected" value="0">Alle</option>
                                    @foreach($brands as $brand)
                                        @if($brand->id  == $brandId)
                                            <option selected="selected" value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endif
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                </select>
                                </td>
                        </tr>
                        <tr>
                            <td><?php echo Form::label('price', 'Tot &euro; ');?></td>
                            <td><?php echo Form::number('priceTo', $priceTo); ?> <br/></td>
                        </tr>
                        <tr>
                            <td><?php echo Form::label('sort', 'Sorteren op');?></td>
                            <td>
                                <select class="form-control" name="filter">
                                    <?php if ($sort =="updated"){ ?>
                                    <option selected="selected" value="updated">Nieuw</option>
                                    <?php } else { ?>
                                    <option value="updated">Nieuw</option>
                                    <?php } ?>
                                    <?php if ($sort =="priceLow"){ ?>
                                    <option selected="selected" value="priceLow">Laagste prijs</option>
                                    <?php } else { ?>
                                    <option value="priceLow">Laagste prijs</option>
                                    <?php } ?>
                                    <?php if ($sort =="priceHigh"){ ?>
                                    <option selected="selected" value="priceHigh">Hoogste prijs</option>
                                    <?php } else { ?>
                                    <option value="priceHigh">Hoogste prijs</option>
                                    <?php } ?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo Form::submit('Filter'); ?></td>
                        </tr>

                    </table>





                {{ Form::close() }}
            </div>

            @foreach(array_chunk((array)$shoes->getItems(), 3) as $threeShoes)
            <div class="row">

                @foreach ($threeShoes as $shoe)

                    <div class="col-sm-4">


                        <a href="{{$shoe->website}}{{ $shoe->url }}">
                            <img src="{{ $shoe->image }}" alt="{{ $shoe->name }}" title="{{ $shoe->name }}" width="277px" height="400px"/>
                        </a>

                        <a href="{{ $shoe->url }}">
                            <p>{{ $shoe->brand->name }}</p>
                            <p>{{ $shoe->name }}</p>
                        </a>
                        <a href="{{$shoe->website}}{{ $shoe->url }}">
                            <?php if ($shoe->sale){?>
                            <p style="color:red">Oude prijs : &euro; {{ number_format($shoe->oldPrice, 2, ',', ''); }}</p>
                            <?php } ?>
                            <p>Prijs : &euro; {{ number_format($shoe->price, 2, ',', ''); }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
            @endforeach
            <div class="pagination"> {{ $shoes->links() }} </div>
            @stop
        </div>
    </div>
</div>




