@extends('layouts.master')


@section('title')
    @parent
    @if ($hasCategory)
        {{$categoryName}} bij goedkope merkschoenen
    @else
        Herenschoenen bij goedkope merkschoenen
    @endif
@stop

@section('content')

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active" id="overview"><a href="{{$baseUrl}}">Alle</a></li>
                @foreach((array)$categories as $category)
                    <li id="category_{{$category->categoryId}}"><a href="{{$baseUrl}}{{$category->path}}/">{{$category->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <div>
                <h1>Herenschoenen <?php if ($hasCategory){echo " > ".$categoryName;}?></h1>
                <div class="form">
                    {{ Form::open(array("url" => "$baseUrl$category->path")) }}
                    <?php echo Form::label('price', 'Van');?>
                    <?php echo Form::number('priceFrom'); ?> <br/>
                    <?php echo Form::label('price', 'Tot');?>
                    <?php echo Form::number('priceTo'); ?> <br/>
                    <?php echo Form::submit('Filter'); ?>
                    {{ Form::close() }}
                </div>

                @foreach(array_chunk((array)$shoes->getItems(), 3) as $threeShoes)
                    <div class="row">

                        @foreach ($threeShoes as $shoe)
                            <div class="col-sm-4">
                                <!--div class="logo">
                            <img src="{{$shoe->logo}}" title="test"/>
                        </div-->

                                <a href="https://www.zalando.be{{ $shoe->url }}">
                                    <img src="{{ $shoe->image }}" alt="{{ $shoe->name }}" title="{{ $shoe->name }}" />
                                </a>

                                <a href="{{ $shoe->url }}">
                                    <p>{{ $shoe->brand }}</p>
                                    <p>{{ $shoe->name }}</p>
                                </a>
                                <a href="https://www.zalando.be{{ $shoe->url }}">
                                    <?php if ($shoe->sale){?>
                                    <p style="color:red">Oude prijs : &euro; {{ $shoe->oldPrice }}</p>
                                    <?php } ?>
                                    <p>Prijs : &euro; {{ $shoe->price }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <div class="pagination"> {{ $shoes->links() }} </div>
                @stop
            </div>
        </div>
    </div>




