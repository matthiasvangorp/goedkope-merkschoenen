<!DOCTYPE html>
<html>
<head>
    <title>
        @section('title')

        @show
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSS are placed here -->
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-theme.css') }}
    {{ HTML::style('css/dashboard.css') }}

    <style>
        @section('styles')
            body {
            padding-top: 60px;
        }
        @show
    </style>
</head>

<body>
<!-- Navbar -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="#">Goedkope merkschoenen</a>
        </div>
        <!-- Everything you want hidden at 940px or less, place within here -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('goedkope_damesschoenen') }}">Damesschoenen</a></li>
                <li><a href="{{ URL::to('goedkope_herenschoenen') }}">Herenschoenen</a></li>
                <!--li><a href="{{ URL::to('goedkope_kinderschoenen') }}">Kinderschoenen</a></li-->
            </ul>
        </div>
    </div>
</div>

<!-- Container -->
<div class="container-fluid">

    <!-- Content -->
    @yield('content')

</div>

<!-- Scripts are placed here -->
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
{{ HTML::script('js/bootstrap.min.js') }}
<script type="text/javascript">
    $(document).ready(function() {
        $('ul li').removeClass('active');
        <?php if ($sidebarId){ ?>
            $('#category_{{$sidebarId}}').addClass('active');
        <?php
            }
            else { ?>
                $('#overview}').addClass('active');
            <?php } ?>

    });
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-62024-42', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>