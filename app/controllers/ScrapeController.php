<?php

class ScrapeController extends BaseController {

	public function index()
	{
        $path = Request::path();
        $arr = explode("/", $path);

        $type = $arr[2];
        //var_dump($arr);
        //die();

        $website = "https://www.zalando.be";

        for ($counter = 1; $counter < 50; $counter++) {
            $scraper = new Scraper();
            $url = $website."/".$type."/?p=".$counter;
            $html = $scraper->scrape($url);


            $parser = new simple_html_dom();
            $parser->load($html);
            $links = $parser->find('a');
            foreach ($links as $link) {
                if ($this->endsWith($link->href, ".html")) {
                    $inDatabase = DB::table('foundUrls')->where('url', $link->href)->get();
                    if (empty($inDatabase)) {
                        if (empty($inDatabase)) {
                            DB::table('foundUrls')->insert(
                                array('url' => $link->href, 'website' => $website)
                            );
                        }
                    }
                } else {
                    $inDatabase = DB::table($type)->where('url', $link->href)->get();
                    if (empty($inDatabase)) {
                        $inDatabase = DB::table('scrapeUrls')->where('url', $link->href)->get();
                        if (empty($inDatabase)) {
                            DB::table('foundUrls')->insert(
                                array('url' => $link->href, 'website' => $website)
                            );
                        }
                    }
                }
                $queries = DB::getQueryLog();
                $last_query = end($queries);


            }
            //die();
            //return View::make('scrape');
        }
	}


    public function scrapeBijenkorf(){
        $scraper = new Scraper();
        $website = "http://www.debijenkorf.be";

        $html = $scraper->scrape("https://www.debijenkorf.be/damesschoenen");
        $parser = new simple_html_dom();
        $parser->load($html);
        $links = $parser->find('div.dbk-product-component a');
        $startPattern = "/\/([a-z])\w+-/";
        $endPattern =   "/\d+$/";
        $refPattern = "/ref=%2F/";
        foreach ($links as $link) {
            $test = $link->href;
            //if (preg_match($startPattern, $link->href) && preg_match($endPattern, $link->href)){
            if (strpos($link->href, "ref=")){
                $inDatabase = DB::table('foundUrls')->where('url', $link->href)->get();
                if (empty($inDatabase)) {
                    if (empty($inDatabase)) {
                        DB::table('foundUrls')->insert(
                            array('url' => $link->href, 'website' => $website)
                        );
                    }
                }
            } else {
                $inDatabase = DB::table('damesschoenen')->where('url', $link->href)->get();
                if (empty($inDatabase)) {
                    $inDatabase = DB::table('scrapeUrls')->where('url', $link->href)->get();
                    if (empty($inDatabase)) {
                        DB::table('scrapeUrls')->insert(
                            array('url' => $link->href, 'website' => $website)
                        );
                    }
                }
            }
        }
    }



	public function scrapeProducts(){


        $website = "https://www.zalando.be";

		$scraper = new Scraper();
		$parser = new simple_html_dom();

		$urls = DB::select('select * from foundUrls order by updated_at asc'); //order by updated_at asc
        //$urls = DB::select('select * from foundUrls order by created_at desc'); //order by created_at desc
		foreach ($urls as $url) {
            $sale = false;
            $oldPrice = 0;

			if ($this->endsWith($url->url, ".html")) {

                    $html = $scraper->scrape($website . $url->url);
                    //$html = $scraper->scrape("https://www.zalando.be/pier-one-veterboots-black-pi912da1j-q11.html");
                    //var_dump($html);
                    $parser->load($html);

                    $mainCategory = $parser->find('[name=header.breadcrumb.1]', 0);
                    if (is_object($mainCategory)) {

                        $mainCategory = $parser->find('[name=header.breadcrumb.1]', 0)->innertext;
                        $subCategory = $parser->find('[name=header.breadcrumb.2]', 0)->innertext;

                        echo "main : $mainCategory<br/>";
                        echo "sub : $subCategory<br/>";

                        if ($subCategory == "Schoenen") {

                            if ($mainCategory == "Dames") {
                                $table = 'damesschoenen';
                            }
                            if ($mainCategory == "Heren") {
                                $table = 'herenschoenen';
                            }
                            if ($mainCategory == "Kinderen") {
                                $table = 'kinderschoenen';
                            }


                            $brand = $parser->find('[itemprop=brand]', 0);
                            if (is_object($brand)) {

                                $brandName = $parser->find('[itemprop=brand]', 0)->innertext;
                                $name = $parser->find('[itemprop=name]', 0)->innertext;
                                if (is_object($parser->find('[itemprop=price]', 0))) {
                                    $price = $parser->find('[itemprop=price]', 0)->innertext;
                                } else {
                                    $price = $parser->find('[id=articlePrice]', 0)->innertext;
                                    $oldPrice = $parser->find('[id=articleOldPrice]', 0)->innertext;
                                    $sale = true;

                                    /*echo "<H1>gevonden</H1>";
                                    echo "oldprice : $oldPrice<br/>";
                                    echo "url : ". $url->url;
                                    die();*/
                                }

                                $category = $parser->find('[name=header.breadcrumb.4]', 0);
                                //var_dump($category);
                                echo $url->url."<br/>";

                                if (!is_object($category)) {
                                    $categoryName = $parser->find('[class=breadcrumbs_item breadcrumbs_item-last]',
                                        0)->innertext;
                                } else {
                                    $categoryName = $parser->find('[name=header.breadcrumb.4]', 0)->innertext;
                                }

                                //die();

                                if (0 === strpos($categoryName, ' <span')) {
                                    $categoryName = $parser->find('[name=header.breadcrumb.3]', 0)->innertext;
                                }

                                echo "categoryname : $categoryName <br/>";
                                $images = $parser->find('img');

                                $found = false;
                                foreach ($images as $image) {
                                    if (!$found) {
                                        if (stristr($image->src, "/detail")) {
                                            $found = true;
                                            $imageUrl = $image->src;
                                        }

                                        if (stristr($image->src, "/brandxl")) {
                                            $logoUrl = $image->src;
                                        }
                                    }
                                    //echo $image->src . "<br/>";
                                }


                                //check if category exists, if not enter it in db
                                $category = DB::table('category')->where('name', $categoryName)->where('type', $table)->get();
                                if (empty($category)) {
                                    $categoryPath = str_replace(' ', '_', $categoryName);
                                    $categoryPath = preg_replace('/[^a-z\d ]/i', '_', strtolower($categoryPath));;
                                    echo "<H1>New category : $categoryName</H1>";

                                    $result = DB::table('category')->insert(
                                        array('name' => $categoryName, 'type' => $table, 'path' => $categoryPath)
                                    );

                                }

                                $category = DB::table('category')->where('name', $categoryName)->get();


                                $singleCategory = $category[0];
                                $singleCategory->id;

                                //check if brand exists, if not enter it in db


                                $brand = DB::table('brand')->where('name', $brandName)->get();
                                if (empty($brand)) {
                                    echo "<h2>new brand : $name</h2>";
                                    $result = DB::table('brand')->insert(
                                        array('name' => $brandName, 'logo' => $logoUrl)
                                    );

                                }

                                $brand = DB::table('brand')->where('name', $brandName)->get();

                                $singleBrand = $brand[0];
                                $singleBrand->id;

                                $singleBrand = $brand[0];
                                //echo "brandId: <br/>";
                                //var_dump($singleBrand);
                                echo " <br/>";
                                echo "brandId : $singleBrand->id <br/>";


                                echo $imageUrl;
                                $priceArray = explode(" ", trim($price));
                                if (sizeof($priceArray) == "") {
                                    var_dump($priceArray);
                                    die();
                                }
                                $price = str_replace(",", ".", $priceArray[1]);

                                $oldPriceArray = explode(" ", trim($oldPrice));
                                if (array_key_exists(1, $oldPriceArray)) {
                                    $oldPrice = str_replace(",", ".", $oldPriceArray[1]);
                                }

                                if ($oldPrice > 0) {
                                    echo "oldprice : $oldPrice<br/>";
                                    echo "url :" . $url->url;
                                    //die();
                                }

                                //var_dump($price);
                                //die();

                                $inDatabase = DB::table($table)->where('url', $url->url)->get();
                                $date = date("Y-m-d H:i:s");
                                if (empty($inDatabase)) {
                                    echo "<H2>New shoe : $name</H2>";
                                    echo "category id : ".$singleBrand->id."<br/>";
                                    $this->insertProduct($table, $singleBrand, $singleCategory, $name, $price, $oldPrice, $sale, $imageUrl, $website, $url);
                                    echo "added to database <br/>";
                                }
                                else {
                                    var_dump($inDatabase);
                                    $result = $inDatabase[0];
                                    $date = date("Y-m-d H:i:s");
                                    $this->updateProduct($table, $result, $name, $price, $oldPrice, $sale, $imageUrl);


                                    //)


                                    echo "updated in database <br/>";
                                    //die();
                                }
                                $queries = DB::getQueryLog();
                                $last_query = end($queries);
                                var_dump($last_query);
                            }
                        }
                    }
                    //die();
			}

            $date = date("Y-m-d H:i:s");
            //DB::enableQueryLog();
            echo "url id : $url->id<br/>";
            echo "date : $date<br/>";

            //update url :
            DB::table('foundUrls')
                ->where('id', $url->id)
                ->update(array('updated_at' => $date));

            //dd(DB::getQueryLog());

		}
		//die();

	}


    public function scrapeBijenkorfProducts()
    {
        $scraper = new Scraper();
        $parser = new simple_html_dom();

        $website = "http://www.debijenkorf.be";

        $oldPrice = 0;

        $urls = DB::select("select * from foundUrls where website = '$website' order by updated_at asc"); //order by created_at desc
        foreach ($urls as $url) {

            $url->url = "/kenzo-espadrille-met-logo-opdruk-3418050001-341805000111360?ref=%2Fdamesschoenen%2Finstapschoenen";


            $sale = false;
            $oldPrice = 0;
            $html = $scraper->scrape($website . $url->url);
            $parser->load($html);

            $breadcrumbs = $parser->find('div.dbk-breadcrumb-toolbar li');
            if (array_key_exists(1, $breadcrumbs)) {
                $mainCategory = $breadcrumbs[2];
                $mainCategory = $mainCategory->innertext;
                $mainCategory = trim(strip_tags($mainCategory));

                if ($mainCategory == "Outlet"){
                    $mainCategory = $breadcrumbs[2];
                    $mainCategory = $mainCategory->innertext;
                    $mainCategory = trim(strip_tags($mainCategory));
                }


                if ($mainCategory == "Dames") {
                    $table = 'damesschoenen';
                }
                if ($mainCategory == "Heren") {
                    $table = 'herenschoenen';
                }
                if ($mainCategory == "Kinderen") {
                    $table = 'kinderschoenen';
                }

                //var_dump($table);
                $breadcrumb = array_pop($breadcrumbs);
                var_dump(trim(strip_tags($breadcrumb->innertext)));

                $subCategory = trim(strip_tags($breadcrumb->innertext));


                echo "sub : $subCategory<br/>";
                $brand = $parser->find('h1.dbk-heading', 0)->innertext;
                $pos = strpos($brand, "<");
                $brandName = trim(substr($brand, 0, $pos));
                var_dump($website . $url->url);
                //die();
                $price = trim($parser->find('span.dbk-price_large', 0)->innertext);




                if (is_object($parser->find('span.dbk-price_old', 0))) {
                    $sale = true;
                    $oldPrice = trim($parser->find('span.dbk-price_old', 0)->innertext);
                    $price = trim($parser->find('span.dbk-price_new', 0)->innertext);
                }

                echo "<br/> oldprice : $oldPrice";
                echo "<br/> price : $price";


                $image = $parser->find('ul.dbk-image-carousel--list img', 0)->src;
                echo $image;

                $name = trim($parser->find('div.dbk-product-summary p', 0)->innertext);
                echo "<br/>name : $name";

                //check if brand category, if not enter it in db
                $category = DB::table('category')->where('name', $subCategory)->get();
                if (empty($category)) {


                    $result = DB::table('category')->insert(
                        array('name' => $subCategory)
                    );
                    $category = DB::table('category')->where('name', $subCategory)->get();
                }

                $singleCategory = $category[0];
                //$singleCategory->categoryId;

                echo "<br/> brand : $brandName";
                echo "category id : $singleCategory->id <br/>";


                //check if brand exists, if not enter it in db
                $brand = DB::table('brand')->where('name', $brandName)->get();
                if (empty($brand)) {
                    $result = DB::table('brand')->insert(
                        array('name' => $brandName, 'logo' => "")
                    );
                    $brand = DB::table('brand')->where('name', $brandName)->get();
                }

                $singleBrand = $brand[0];
                //$singleBrand->brandId;

                $inDatabase = DB::table($table)->where('url', $url->url)->get();

                if (empty($inDatabase)) {
                    $this->insertProduct($table, $singleBrand, $singleCategory, $name, $price, $oldPrice, $sale, $imageUrl, $website, $url);
                    echo "added to database <br/>";
                }
                else {
                    $result = $inDatabase[0];
                    $this->updateProduct($table, $result, $name, $price, $oldPrice, $sale, $imageUrl);

                    echo "updated in database <br/>";
                }
            }

        }


    }

	function endsWith($haystack, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}

		return (substr($haystack, -$length) === $needle);
	}


	function insertProduct($table, $singleBrand, $singleCategory, $name, $price, $oldPrice, $sale, $imageUrl, $website, $url){
        $date = date("Y-m-d H:i:s");
        DB::table($table)->insert(
            array(
                'brandId' => $singleBrand->id,
                'categoryId' => $singleCategory->id,
                'name' => $name,
                'price' => $price,
                'oldPrice' => $oldPrice,
                'sale' => $sale,
                'image' => $imageUrl,
                'website' => "https://www.zalando.be",
                'url' => $url->url,
                'updated_at' => $date
            )
        );
    }

    private function updateProduct($table, $result, $name, $price, $oldPrice, $sale)
    {
        $date = date("Y-m-d H:i:s");
        DB::table($table)->where('id', $result->id)
            ->update(array(
                'name' => $name,
                'price' => $price,
                'oldPrice' => $oldPrice,
                'sale' => $sale,
                'image' => $imageUrl,
                'updated_at' => $date
            ));
    }

}
