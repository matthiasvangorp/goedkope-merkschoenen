<?php

//namespace App;


//use Paginator;
//use Illuminate\Http\Request;

class DamesSchoenenController extends BaseController {

	public function index($path = null)
	{
        $sidebarId = false;
        $categoryName = "";
		$itemsPerPage = 21;
		$page = Input::get('page');
		if ($page == ""){
			$page = 1;
		}

        $priceFrom = 0;
        $priceTo = 10000;
        $brandId = 0;
        $sort = "";
        $sortField = "created_at";
        $order = "DESC";

        if (Request::isMethod('post'))
        {
            $priceFrom = Input::get('priceFrom', 0);
            if ($priceFrom == ''){
                $priceFrom = 0;
            }
            $priceTo = Input::get('priceTo', 10000);
            if ($priceTo == ''){
                $priceTo = 10000;
            }
            $brandId = (int)Input::get('brandId', 0);
            $sort = Input::get('filter','');



            switch ($sort){
                case "updated":
                    $sortField = "updated_at";
                    $order = "ASC";
                    break;
                case "priceLow":
                    $sortField = "price";
                    $order = "ASC";
                    break;
                case "priceHigh":
                    $sortField = "price";
                    $order = "DESC";
                    break;

            }

            echo "brandid : $brandId";

        }

        $start = ($page - 1) * $itemsPerPage;

        $path = Request::path();
        $arr = explode("/", $path);

        if ($arr[0] == '' && $arr[1] == ''){
            $arr = [];
        }

        $categoryPath = "";

        switch (sizeof($arr)) {
            case 0:
                $path = "";
                $hasCategory = false;
                $query = WomenShoes::whereBetween('price',array( $priceFrom,  $priceTo))->limit($itemsPerPage)->orderBy($sortField, $order)->offset($start);
                if ($brandId > 0){
                    $query->where('brandId', $brandId);
                }
                $shoes = $query->paginate($itemsPerPage);
                $title = "Damesschoenen";
                break;
            case 1:

                $path = $arr[0];
                $categoryPath = "";
                $hasCategory = false;
                if ($path == Config::get('constants.DAMESSCHOENEN_URL')) {
                    $query = WomenShoes::whereBetween('price',array( $priceFrom,  $priceTo))->limit($itemsPerPage)->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Damesschoenen";
                }
                if ($path == Config::get('constants.HERENSCHOENEN_URL')) {
                    $query = MenShoes::whereBetween('price',array( $priceFrom,  $priceTo))->limit($itemsPerPage)->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Herenschoenen";

                }

                if ($path == Config::get('constants.KINDERSCHOENEN_URL')) {
                    $query = ChildrenShoes::whereBetween('price',array( $priceFrom,  $priceTo))->limit($itemsPerPage)->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Kinderschoenen";
                }
                break;
            case 2 :
                $path = $arr[0];
                $categoryPath = $arr[1];
                //get categoryID
                $categoryId = DB::select("select id, name from category where category.path = '$categoryPath'");
                $hasCategory = true;


                //get the category id to make the right sidebar-menu-item active.

                $sidebarId = $categoryId[0]->id;
                $categoryName = $categoryId[0]->name;
                if ($path == Config::get('constants.DAMESSCHOENEN_URL')) {
                    $query = WomenShoes::whereBetween('price', array($priceFrom, $priceTo))
                        ->where("categoryId", "=", $categoryId[0]->id)->limit($itemsPerPage)
                        ->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Damesschoenen";
                }

                if ($path == Config::get('constants.HERENSCHOENEN_URL')) {
                    $query = MenShoes::whereBetween('price', array($priceFrom, $priceTo))
                        ->where("categoryId", "=", $categoryId[0]->id)->limit($itemsPerPage)
                        ->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Herenschoenen";
                }

                if ($path == Config::get('constants.KINDERCHOENEN_URL')) {
                    $query = ChildrenShoes::whereBetween('price', array($priceFrom, $priceTo))
                        ->where("categoryId", "=", $categoryId[0]->id)->limit($itemsPerPage)
                        ->orderBy($sortField, $order)->offset($start);
                    if ($brandId > 0){
                        $query->where('brandId', $brandId);
                    }
                    $shoes = $query->paginate($itemsPerPage);
                    $title = "Kinderschoenen";
                }
        }


        if ($path != ""){
            $baseUrl = Config::get('app.url')."/".$path."/";
        }
        else {
            $baseUrl = Config::get('app.url')."/goedkope_damesschoenen/";
        }

        $queries = DB::getQueryLog();
        $last_query = end($queries);




        //get the categories
        $categories = Category::select()
            ->whereIn('id', function ($query) {
                $query->distinct('categoryId')->select('categoryId')
                    ->from("damesschoenen");
            })
            ->where('type','damesschoenen')
            ->orderBy('name', 'asc')
            ->get();

        if ($path == Config::get('constants.HERENSCHOENEN_URL')) {
            $categories = Category::select()
                ->whereIn('id', function ($query) {
                    $query->distinct('categoryId')->select('categoryId')
                        ->from("herenschoenen");
                })
                ->where('type','herenschoenen')
                ->orderBy('name', 'asc')
                ->get();
        }
        if ($path == Config::get('constants.KINDERSCHOENEN_URL')) {
            $categories = Category::select()
                ->whereIn('id', function ($query) {
                    $query->distinct('categoryId')->select('categoryId')
                        ->from("herenschoenen");
                })
                ->where('type','herenschoenen')
                ->orderBy('name', 'asc')
                ->get();
        }


        //get the brands
        $brands = Brand::select()->orderBy('name')->get();




        $queries = DB::getQueryLog();
        $last_query = end($queries);




		//$paginator = Paginator::make($shoes, $numberOfShoes, $itemsPerPage);
		return View::make('damesschoenen', array(
            //"shoes" => $paginator,
            "shoes" => $shoes,
            "categories" => $categories,
            "brands" => $brands,
            "baseUrl" => $baseUrl,
            "hasCategory" => $hasCategory,
            "sidebarId" => $sidebarId,
            "categoryName" => $categoryName,
            "title" => $title,
            "brandId" => $brandId,
            "categoryPath" => $categoryPath,
            "sort"          => $sort,
            "priceFrom"     => $priceFrom,
            "priceTo"       => $priceTo
        ));
	}
}

/*
 * handig :
 *
    $queries = DB::getQueryLog();
    $last_query = end($queries);
    var_dump($last_query);
    die();
*/
