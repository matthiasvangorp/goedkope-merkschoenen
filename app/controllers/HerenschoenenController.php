<?php

class HerenschoenenController extends BaseController {

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function index()
	{

		$table = Config::get('constants.HERENSCHOENEN_TABLE');
		$environment = App::environment();
		$sidebarId = false;
		$categoryName = "";
		$itemsPerPage = 21;
		$page = Input::get('page');
		if ($page == ""){
			$page = 1;
		}

		$priceFrom = 0;
		$priceTo = 10000;
		if (Request::isMethod('post')){
			$priceFrom = Input::get('priceFrom', 0);
			$priceTo = Input::get('priceTo', 10000);
		}


		$baseUrl = Config::get('app.url').Request::segment(1)."/";

		$categoryPath = Request::segment(2);



		$categories = DB::select("select * from category where categoryId in (select distinct(categoryId) from $table) order by name ");




		$start = ($page - 1) * $itemsPerPage;

		if ($categoryPath == "") {
			$hasCategory = false;
			$numberOfShoes = DB::select("select count(productId) as number from $table");
			$shoes = DB::select("select d.name, d.image, d.price, d.sale, d.oldPrice,  d.url, b.logo, b.name
                                  as brand from $table d, brand b where d.brandId = b.brandId and
                                  d.price between $priceFrom and $priceTo order by d.created_at desc
                                  limit $start, $itemsPerPage");
		}
		else {
			$hasCategory = true;
			$shoes = DB::select("select d.name, d.image, d.price, d.sale, d.oldPrice,  d.url, b.logo,
                                          b.name as brand from $table d, brand b, category c where
                                          d.brandId = b.brandId and c.path = '$categoryPath' and
                                          d.categoryId = c.categoryId and d.price between $priceFrom and
                                          $priceTo order by d.created_at desc limit $start, $itemsPerPage");
			$numberOfShoes = DB::select("select count(productId) as number from $table d, category c where d.categoryId =
                                          c.categoryId and c.path = '$categoryPath' ");

			//get the category id to make the right sidebar-menu-item active.
			$categoryId = DB::select("select categoryId, name from category where category.path = '$categoryPath'");
			$sidebarId = $categoryId[0]->categoryId;
			$categoryName = $categoryId[0]->name;
		}

		$numberOfShoes = $numberOfShoes[0]->number;

		$paginator = Paginator::make($shoes, $numberOfShoes, $itemsPerPage);
		return View::make('herenschoenen', array(
			"shoes" => $paginator,
			"categories" => $categories,
			"baseUrl" => $baseUrl,
			"hasCategory" => $hasCategory,
			"sidebarId" => $sidebarId,
			"categoryName" => $categoryName
		));
	}

}
