<?php
class Scraper{
    
    public function __construct(){
        // set proxies -- you can add your own here or use the setProxies method
        $this->_proxies = array();
    }
    
    public function scrape($url)
    {
        $this->_url = $url;
        $dom = new DOMDocument();
    	$proxy = $this->_pickProxy();
    	
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_REFERER, "");
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.151 Safari/535.19");
    	if($proxy){
        	curl_setopt($ch, CURLOPT_PROXY, $proxy); 
        	curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
    	}
    	$body = curl_exec($ch);
    	curl_close($ch);
    	
    	/*->_curl_result = $body;
    	@$dom->loadHTML($body);
    	$this->_dom = $dom;

    	$this->_parseDOM();	*/
        
        return $body;
    }
    
    public function setProxies($proxies)
    {
        $this->_proxies = $proxies;
    }
    
    private function _pickProxy()
    {
        if(count($this->_proxies) > 0)
            return $this->_proxies[rand(0, count($this->_proxies) - 1)];
        else return false;
    }
    
    public function setKeyword($keyword)
    {
        $this->_keyword = $keyword;
    }
    
    
}