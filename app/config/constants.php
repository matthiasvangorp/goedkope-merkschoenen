<?php
//file : app/config/constants.php

return [
    'DAMESSCHOENEN_TABLE' => 'damesschoenen',
    'HERENSCHOENEN_TABLE' => 'herenschoenen',
    'KINDERSCHOENEN_TABLE' => 'kinderschoenen',
    'DAMESSCHOENEN_URL'   => 'goedkope_damesschoenen',
    'HERENSCHOENEN_URL'   => 'goedkope_herenschoenen',
    'KINDERSCHOENEN_URL'   => 'goedkope_kinderschoenen'
];