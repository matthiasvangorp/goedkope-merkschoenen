<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('',  'DamesschoenenController@index');
Route::get('goedkope_damesschoenen/{path?}/{page?}', 'DamesSchoenenController@index');
Route::post('goedkope_damesschoenen/{path?}/{page?}', 'DamesSchoenenController@index');
Route::get('goedkope_herenschoenen/{path?}/{page?}',  'DamesSchoenenController@index');
Route::post('goedkope_herenschoenen/{path?}/{page?}', 'DamesSchoenenController@index');
Route::get('goedkope_kinderschoenen/{path?}/{page?}', 'DamesSchoenenController@index');
Route::post('goedkope_kinderschoenen/{path?}/{page?}', 'DamesSchoenenController@index');
//Route::get('scrape',                  'ScrapeController@index');
//Route::get('scrape-products',         'ScrapeController@scrapeProducts');
//Route::get('scrape-bijenkorf-products',        'ScrapeController@scrapeBijenkorfProducts');
Route::get('scrape/scrape/{path?}', [
    'as' => 'scrape', 'uses' => 'ScrapeController@index'
]);
Route::get('scrape/scrape-bijenkorf',  [
     'uses' => 'ScrapeController@scrapeBijenkorf'
]);
Route::get('scrape/scrape-products',  [
     'uses' => 'ScrapeController@scrapeProducts'
]);
Route::get('scrape/scrape-bijenkorf-products',  [
    'uses' => 'ScrapeController@scrapeBijenkorfProducts'
]);