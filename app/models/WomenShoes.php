<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class WomenShoes extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'damesschoenen';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//public static $timestamps = true;


	public function brand(){
		return $this->belongsTo('Brand', 'brandId');
	}

	public function category(){
		return $this->belongsTo('Category', 'categoryId');
	}



}
